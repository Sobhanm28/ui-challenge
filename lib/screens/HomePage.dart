import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ui_challenge/screens/MyList.dart';
import 'package:ui_challenge/screens/Profile.dart';
import 'package:ui_challenge/screens/Search.dart';
import 'package:ui_challenge/widgets/HomePageBottomBar.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _pageIndex=0;
  StreamController _pageListener=StreamController.broadcast();
  PageController _pageController=PageController();
  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            PageView(
              onPageChanged: (pageIndex){
                _pageListener.add(pageIndex);
              },
              controller: _pageController,
              children: [
                MyList(),
                Search(),
                Profile(),
              ],
            ),
            HomePageBottomBar(
              pageController: _pageController,
              pageListener: _pageListener,
            )
          ],
        ),
      ),
    );
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ui_challenge/domain/Meal.dart';
import 'package:ui_challenge/domain/PageType.dart';
import 'package:ui_challenge/widgets/MealList.dart';
import 'package:ui_challenge/widgets/MineralGroup.dart';
import 'package:ui_challenge/widgets/SearchBox.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  List _searchList = [];
  List _mealList = [];
  StreamController _mineralGroupListener = StreamController.broadcast();

  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  void initState() {
    super.initState();
    getList();
  }

  getList() {
    for (int i = 0; i < data.length; i++) {
      for (int j = 0; j < data[i]["data"].length; j++) {
        _mealList.add(data[i]["data"][j]);
        _searchList.add(data[i]["data"][j]);
      }
    }
    setState(() {});
  }

  search(String v) {
    _searchList.clear();
    _mealList.forEach((element) {
      if (element["describe"].toString().toLowerCase().contains(v) ||
          element["name"].toString().toLowerCase().contains(v)) {
        _searchList.add(element);
      }
    });
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 0,
                      child: Container(
                width: width(1, context),
                height: height(0.25, context),
                color: Color(0xff45B25E),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(
                      height: height(0.02, context),
                    ),
                    SearchBox(
                      search: search,
                    ),
                    MineralGroup(
                      mineralGroupListener: _mineralGroupListener,
                    )
                  ],
                )),
          ),
          Positioned(
            top: height(0.25, context),
                      child: Container(
              padding: EdgeInsets.only(bottom: height(0.125, context)),
                width: width(1, context),
                height: height(0.75, context),
                child: SingleChildScrollView(
                                  child: MealList(
                    data: _searchList,
                    pageType: PageType.Search,
                    mineralGroupListener: _mineralGroupListener,
                  ),
                ),
              ),
          ),
          Positioned(
            bottom:height(0.15, context) ,
            left: 0,
            right: 0,
            child: Container(
              alignment: Alignment.center,
              height: height(0.06, context),
              
              child: Container(
                 width: width(0.3, context),
              height: height(0.06, context),
              decoration: BoxDecoration(
              color: Color(0xff45B25E),

                borderRadius: BorderRadius.all(Radius.circular(30)),
                
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(Icons.tune,color: Colors.white,size: 25,),
                  SizedBox(width: 5,),
                  Text("Filter",style: TextStyle(color: Colors.white,fontSize: 18),)
                ],
              ),
              ),
            ))
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:ui_challenge/widgets/LoginButton.dart';
import 'package:ui_challenge/widgets/LoginForgotPass.dart';
import 'package:ui_challenge/widgets/LoginTextField.dart';
import 'package:ui_challenge/widgets/LoginTitle.dart';
import 'package:ui_challenge/widgets/TermsOfPolicy.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  width(double d) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: width(1),
        height: height(1),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              LoginTitle(),
              LoginTextField(
                title: "Email",
                hasIcon: false,
              ),
              LoginTextField(
                title: "Password",
                hasIcon: true,
              ),
              LoginForgotPass(),
              LoginButton(
                text: "Login",
                textColor: Colors.white,
                backgroundColor: Color(0xff45B25E),
              ),
              LoginButton(
                text: "Dont Have an Account Yet?",
                textColor: Colors.green,
                backgroundColor: Color(0xffECF8EF),
              ),
              TermsOfPolicy()
            ],
          ),
        ),
      ),
    );
  }
}

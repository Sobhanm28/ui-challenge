import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ui_challenge/domain/Meal.dart';
import 'package:ui_challenge/domain/PageType.dart';
import 'package:ui_challenge/widgets/FloatingSelectionList.dart';
import 'package:ui_challenge/widgets/HeadList.dart';
import 'package:ui_challenge/widgets/MealList.dart';
import 'package:ui_challenge/widgets/MyListAppBar.dart';

class MyList extends StatefulWidget {
  @override
  _MyListState createState() => _MyListState();
}

class _MyListState extends State<MyList> {
  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  refresh(_) {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: height(1, context),
        child: Stack(
          children: [
            MyListAppBar(),
            Container(
                margin: EdgeInsets.symmetric(
                  vertical: height(0.125, context),
                ),
                child: SingleChildScrollView(
                  child: Column(
                    children: List.generate(
                        data.length,
                        (index) => Column(
                              children: [
                                HeadList(
                                  index: index,
                                ),
                                MealList(
                                  data: data[index]["data"],
                                  refresh: refresh,
                                  pageType: PageType.MyList,
                                )
                              ],
                            )),
                  ),
                )),
         FloatingSelectionList()
          ],
        ),
      ),
    );
  }
}

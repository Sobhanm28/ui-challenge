import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ui_challenge/screens/HomePage.dart';
import 'package:ui_challenge/screens/Login.dart';

void main() {

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
         SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitDown,
        DeviceOrientation.portraitUp,

      ]);
    return MaterialApp(
      title: 'Ui Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
        fontFamily: "Montserrat",
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home:Login(),
    );
  }
}



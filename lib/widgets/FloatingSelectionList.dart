import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class FloatingSelectionList extends StatelessWidget {
  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: height(0.15, context),
      left: 0,
      right: 0,
      child: Container(
        alignment: Alignment.center,
        height: height(0.07, context),
        child: Container(
          width: width(0.85, context),
          height: height(0.08, context),
          decoration: BoxDecoration(
              color: Color(0xff45B25E),
              borderRadius: BorderRadius.all(Radius.circular(50))),
          child: Row(
            children: [
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.all(5),
                width: width(0.4, context),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(50))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.star,
                      color: Color(0xff45B25E),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      "Green",
                      style: TextStyle(color: Color(0xff45B25E)),
                    )
                  ],
                ),
              ),
              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.all(5),
                width: width(0.325, context),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(50))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.cancel,
                      color: Colors.white70,
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Text(
                      "Black",
                      style: TextStyle(color: Colors.white70),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

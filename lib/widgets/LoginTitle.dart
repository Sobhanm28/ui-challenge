import 'package:flutter/material.dart';

class LoginTitle extends StatelessWidget {
  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: height(0.07, context)
        ),
      child: Text(
        "Login To Your Account",
        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
      ),
    );
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ui_challenge/domain/PageType.dart';

class MealList extends StatefulWidget {
  final List data;
  final ValueChanged<void> refresh;
  final PageType pageType;
  final StreamController mineralGroupListener;

  const MealList(
      {Key key,
      this.data,
      this.refresh,
      this.pageType,
      this.mineralGroupListener})
      : super(key: key);

  @override
  _MealListState createState() => _MealListState();
}

class _MealListState extends State<MealList> {
  int mineralIndex = 5;
  int selectedMealItem;
  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  List mineralColor = [
    Colors.red,
    Colors.orange,
    Colors.green,
    Colors.green,
    Colors.orange,
  ];

  BoxDecoration myListStyle(int index) {
    return BoxDecoration(
        shape: BoxShape.circle,
        color: index == 0
            ? Colors.red
            : index == 1
                ? Colors.orange
                : index == 2
                    ? Colors.green
                    : index == 3
                        ? Colors.green
                        : index == 4 ? Colors.orange : Colors.grey);
  }

  BoxDecoration searchStyle(int index) {
    return BoxDecoration(
        shape: BoxShape.circle,
        color:
            index == mineralIndex ? mineralColor[mineralIndex] : Colors.grey);
  }

  Row mineralsitem(String image, String text, int index, BuildContext context) {
    return Row(
      children: [
        Container(
          alignment: Alignment.center,
          width: width(0.04, context),
          height: height(0.03, context),
          decoration: widget.pageType == PageType.MyList
              ? myListStyle(index)
              : searchStyle(index),
          child: Image.asset(
            "assets/images/$image.png",
            width: width(0.0225, context),
            height: height(0.0125, context),
            fit: BoxFit.fill,
          ),
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          text,
          style: TextStyle(fontSize: 10, color: Colors.grey),
        )
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    if (widget.pageType == PageType.Search) {
      widget.mineralGroupListener.stream.listen((index) {
        setState(() {
          mineralIndex = index;
        });
        print("mineral index :$mineralIndex");
        print(mineralColor[index]);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: List.generate(widget.data.length, (index) {
        final item = widget.data[index];
        return Column(
          children: [
            GestureDetector(
              onTap: (){
                   if (widget.pageType == PageType.Search) {
                      print(index);
                      setState(() {
                        selectedMealItem = index;
                      });
                    }
              },
                          child: AbsorbPointer(
                absorbing:widget.pageType==PageType.Search? true:false,
                            child: Dismissible(
                  key: Key(item.toString()),
                  
                  direction: DismissDirection.endToStart,
                  background: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    color: Colors.red,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.delete,
                          color: Colors.white,
                        ),
                        Icon(Icons.delete, color: Colors.white),
                      ],
                    ),
                  ),
                  onDismissed: (onDissmiss) {
                    widget.refresh(null);
                    widget.data.removeAt(index);
                  },
                  child: Container(
                    margin: EdgeInsets.fromLTRB(15, 15, 0, 15),
                    child: Row(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              border: Border.all(
                                  width: 2,
                                  color: widget.pageType == PageType.MyList
                                      ? Colors.green
                                      : selectedMealItem == index
                                          ? Colors.red
                                          : Colors.grey),
                              borderRadius: BorderRadius.all(Radius.circular(5))),
                          child: ClipRRect(
                            borderRadius: BorderRadius.all(Radius.circular(4)),
                            child: Image.asset(
                              "assets/images/" +
                                  widget.data[index]["image"] +
                                  ".jpg",
                              height: height(0.1, context),
                              width: width(0.175, context),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 15),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget.data[index]["name"],
                                style:
                                    TextStyle(color: Colors.grey, fontSize: 13),
                              ),
                              SizedBox(
                                height: 7.5,
                              ),
                              Text(
                                widget.data[index]["describe"],
                                style: TextStyle(
                                    fontSize: 13, fontWeight: FontWeight.w600),
                              ),
                              SizedBox(
                                height: 7.5,
                              ),
                              Container(
                                width: width(0.7, context),
                                child: SingleChildScrollView(
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: List.generate(
                                        5,
                                        (i) => mineralsitem(
                                            widget.data[index]["mineral"][i]
                                                ["image"],
                                            widget.data[index]["mineral"][i]
                                                ["text"],
                                            i,
                                            context)),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: width(1, context),
              height: 0.5,
              color: Colors.grey[400],
            )
          ],
        );
      }),
    );
  }
}

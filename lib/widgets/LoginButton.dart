import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ui_challenge/screens/HomePage.dart';

class LoginButton extends StatelessWidget {
  final String text;
  final Color textColor;
  final Color backgroundColor;

  const LoginButton({Key key, this.text, this.textColor, this.backgroundColor})
      : super(key: key);

  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.push(context,MaterialPageRoute(builder: (context)=>HomePage()));
      },
          child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.symmetric(
          vertical: height(0.008, context)
        ),
        child: Text(
          text,
          style: TextStyle(color: textColor),
        ),
        width: width(0.88, context),
        height: height(0.08, context),
        decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: BorderRadius.all(Radius.circular(10))),
      ),
    );
  }
}

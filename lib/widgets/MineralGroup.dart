import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MineralGroup extends StatefulWidget {
  final StreamController mineralGroupListener;

  const MineralGroup({Key key, this.mineralGroupListener}) : super(key: key);
  @override
  _MineralGroupState createState() => _MineralGroupState();
}

class _MineralGroupState extends State<MineralGroup> {
  List iconsData = ["sugar", "cal", "salt", "fibre", "cir"];
  int selectedIndex;

  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  mineralsitem(String text, BuildContext context, int index) {
    return GestureDetector(
      onTap: () {
        widget.mineralGroupListener.add(index);
        setState(() {
          selectedIndex = index;
        });
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 300),
        curve: Curves.fastOutSlowIn,
        margin: EdgeInsets.only(left: 10),
        width:
            selectedIndex == index ? width(0.3, context) : width(0.2, context),
        height: height(0.0525, context),
        decoration: BoxDecoration(
            color: selectedIndex == index ? Colors.white : Colors.white30,
            borderRadius: BorderRadius.all(Radius.circular(7.5))),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                alignment: Alignment.center,
                width: width(0.04, context),
                height: height(0.03, context),
                decoration: BoxDecoration(
                    color: selectedIndex == index
                        ? Color(0xff388E4C)
                        : Colors.white,
                    shape: BoxShape.circle),
                child: Image.asset(
                  selectedIndex == index
                      ? "assets/images/$text.png"
                      : "assets/images/fill-$text.png",
                  width: width(0.0225, context),
                  height: height(0.0125, context),
                  fit: BoxFit.fill,
                ),
              ),
              Text(
                text,
                style: TextStyle(
                    fontSize: 12,
                    color: selectedIndex == index
                        ? Color(0xff388E4C)
                        : Colors.white),
              ),
              Center(
                  child: selectedIndex == index
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              color: Colors.grey[500],
                              width: 1,
                              height: height(0.02, context),
                            ),
                            Icon(Icons.sort, color: Color(0xff388E4C), size: 18)
                          ],
                        )
                      : Container())
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
          children: List.generate(
              5, (index) => mineralsitem(iconsData[index], context, index))),
    );
  }
}

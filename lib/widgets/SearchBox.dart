import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ui_challenge/domain/Meal.dart';

class SearchBox extends StatefulWidget {

  final ValueChanged<String> search;


   SearchBox({Key key, this.search}) : super(key: key);

  @override
  _SearchBoxState createState() => _SearchBoxState();
}

class _SearchBoxState extends State<SearchBox> {
  TextEditingController _searchController = TextEditingController();

  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          width: width(0.75, context),
          height: height(0.07, context),
          decoration: BoxDecoration(
              color: Color(0xff388E4C),
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  child: Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 22,
                  ),
                ),
              ),
              Expanded(
                  flex: 4,
                  child: Container(
                    alignment: Alignment.center,
                    child: TextField(
                      controller: _searchController,
                      cursorColor: Colors.white,
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                       
                      ),
                      onChanged: (v) {
                        widget.search(v.toLowerCase());
                      
                      },
                    ),
                  )),
              Expanded(
                  flex: 1,
                  child: GestureDetector(
                    onTap: () {
                      _searchController.text = "";
                    },
                    child: Icon(
                      Icons.cancel,
                      color: Colors.white70,
                      size: 18,
                    ),
                  ))
            ],
          ),
        ),
        GestureDetector(
          onTap: () {
            _searchController.text = "";
          },
          child: Text(
            "Cancel",
            style: TextStyle(color: Colors.white),
          ),
        )
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MyListAppBar extends StatelessWidget {
  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0,
          child: Container(
        padding: EdgeInsets.only(bottom: 15),
        alignment: Alignment.bottomCenter,
        height: height(0.125, context),
        width: width(1, context),
        color: Color(0xff46B25F),
        child: Text(
          "My Green List",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
      ),
    );
  }
}

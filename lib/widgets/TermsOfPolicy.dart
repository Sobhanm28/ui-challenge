import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TermsOfPolicy extends StatelessWidget {

    width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }


  
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: height(0.03, context)),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
            text: "By continuing, you agree to Eatiquette's \n",
            style: TextStyle(color: Colors.grey, fontFamily: "Montserrat"),
            children: <TextSpan>[
              TextSpan(
                  text: "Terms Of Use ", style: TextStyle(color: Colors.green)),
              TextSpan(text: "and ", style: TextStyle(color: Colors.grey)),
              TextSpan(
                  text: "Privacy Policy",
                  style: TextStyle(color: Colors.green)),
            ]),
      ),
    );
  }
}

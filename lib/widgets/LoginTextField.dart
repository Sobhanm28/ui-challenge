import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class LoginTextField extends StatefulWidget {
  final String title;
  final bool hasIcon;

  const LoginTextField({Key key, this.title, this.hasIcon}) : super(key: key);

  @override
  _LoginTextFieldState createState() => _LoginTextFieldState();
}

class _LoginTextFieldState extends State<LoginTextField> {
  bool passVisible=true;

  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(primaryColor: Colors.green),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: height(0.008, context)),
        width: width(0.88, context),
        child: TextField(
          obscureText:passVisible? false:true,
          decoration: InputDecoration(
              labelText: widget.title,
              border: OutlineInputBorder(),
              suffixIcon: GestureDetector(
                onTap: (){
                  setState(() {
                    passVisible=!passVisible;
                  });
                },
                child: Icon(widget.hasIcon?passVisible?Icons.visibility:Icons.visibility_off:null)),
              labelStyle: TextStyle(fontFamily: "Montserrat")),
        ),
      ),
    );
  }
}
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HomePageBottomBar extends StatefulWidget {
  final PageController pageController;
  final StreamController pageListener;

  const HomePageBottomBar({Key key, this.pageController, this.pageListener})
      : super(key: key);

  @override
  _HomePageBottomBarState createState() => _HomePageBottomBarState();
}

class _HomePageBottomBarState extends State<HomePageBottomBar> {
  int _index = 0;
  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      widget.pageListener.stream.listen((pageIndex) {
        setState(() {
          _index = pageIndex;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      child: Container(
        decoration: BoxDecoration(color: Colors.white, boxShadow: [
          BoxShadow(color: Colors.grey, spreadRadius: 0.1, blurRadius: 5)
        ]),
        height: height(0.125, context),
        width: width(1, context),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              onTap: () {
                setState(() {
                  _index = 0;
                });
                widget.pageController.animateToPage(0,
                    duration: Duration(milliseconds: 300),
                    curve: Curves.fastOutSlowIn);
              },
              child: Container(
                height: height(0.08, context),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Center(
                      child: _index == 0
                          ? Container(
                              width: 3,
                              height: 3,
                              decoration: BoxDecoration(
                                  color: Colors.green, shape: BoxShape.circle),
                            )
                          : Container(),
                    ),
                    Icon(
                      Icons.format_list_bulleted,
                      size: 30,
                      color: _index == 0 ? Colors.green : Colors.grey,
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _index = 1;
                });
                widget.pageController.animateToPage(1,
                    duration: Duration(milliseconds: 300),
                    curve: Curves.fastOutSlowIn);
              },
              child: AnimatedContainer(
                duration: Duration(milliseconds: 200),
                curve: Curves.linear,
                width: _index == 1 ? width(0.3, context) : width(0.15, context),
                height:
                    _index == 1 ? height(0.08, context) : height(0.07, context),
                decoration: BoxDecoration(
                    color: _index == 1 ? Color(0xffECF7EF) : Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(30))),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: width(0.15, context),
                        height: height(0.07, context),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color:
                                _index == 1 ? Color(0xff46B25F) : Colors.white),
                        child: Icon(
                          Icons.search,
                          color: _index == 1 ? Colors.white : Colors.grey,
                          size: 30,
                        ),
                      ),
                      Container(
                        width: width(0.15, context),
                        height: height(0.07, context),
                        decoration: BoxDecoration(shape: BoxShape.circle),
                        child: Icon(
                          Icons.wallpaper,
                          color: Color(0xff46B25F),
                          size: 20,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  _index = 2;
                });
                widget.pageController.animateToPage(2,
                    duration: Duration(milliseconds: 300),
                    curve: Curves.fastOutSlowIn);
              },
              child: Container(
                height: height(0.08, context),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Center(
                      child: _index == 2
                          ? Container(
                              width: 3,
                              height: 3,
                              decoration: BoxDecoration(
                                  color: Colors.green, shape: BoxShape.circle),
                            )
                          : Container(),
                    ),
                    Icon(Icons.person_outline,
                        size: 30,
                        color: _index == 2 ? Colors.green : Colors.grey),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

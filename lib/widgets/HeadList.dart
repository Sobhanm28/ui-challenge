import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:ui_challenge/domain/Meal.dart';

class HeadList extends StatelessWidget {
  final int index;

  const HeadList({Key key, this.index}) : super(key: key);


   width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      width: width(1, context),
      height: height(0.06, context),
      color: Color(0xffE7F2E9),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            data[index]["group"],
            style: TextStyle(color: Color(0xff46B25F)),
          ),
          Text(""),
          Row(
            children: [
              Container(
                  alignment: Alignment.center,
                  width: 25,
                  height: 15,
                  decoration: BoxDecoration(
                      color: Color(0xff46B25F),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  child: Text(data[index]["data"].length.toString(),
                      style: TextStyle(color: Colors.white, fontSize: 12))),
              SizedBox(
                width: 7.5,
              ),
              Icon(
                Icons.keyboard_arrow_down,
                color: Color(0xff46B25F),
                size: 20,
              )
            ],
          )
        ],
      ),
    );
  }
}

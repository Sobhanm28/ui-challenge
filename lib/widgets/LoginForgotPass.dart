import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class LoginForgotPass extends StatelessWidget {
  width(double d, BuildContext context) {
    return MediaQuery.of(context).size.width * d;
  }

  height(double d, BuildContext context) {
    return MediaQuery.of(context).size.height * d;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: height(0.01, context),
        bottom: height(0.03, context)
      ),
      width: width(0.88, context),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            "Forgot Your Password?",
            style: TextStyle(color: Colors.grey),
          )
        ],
      ),
    );
  }
}

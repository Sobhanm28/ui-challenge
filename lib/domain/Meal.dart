List data = [
  {
    "group": "Ice Cream",
    "data": [
      {
        "name": "Nestle",
        "describe": "Slow Churned Rocky Road Cups",
        "image": "ice1",
        "mineral": [
          {"image": "sugar", "text": "38.6 g"},
          {"image": "cal", "text": "506 kal"},
          {"image": "salt", "text": "2 g"},
          {"image": "fibre", "text": "4.8 g"},
          {"image": "cir", "text": "19"}
        ],
      },
      {
        "name": "Nestle",
        "describe": "Slow Churned Rocky Road Cups",
        "image": "ice2",
        "mineral": [
          {"image": "sugar", "text": "40.3 g"},
          {"image": "cal", "text": "410 kal"},
          {"image": "salt", "text": "2 g"},
          {"image": "fibre", "text": "4 g"},
          {"image": "cir", "text": "19"}
        ],
      },
      
    ]
  },
  {
    "group": "Bread & Bakery",
    "data": [
      {
        "name": "Slow Tony",
        "describe": "Wheat Bread With Cranberries",
        "image": "bead",
        "mineral": [
          {"image": "sugar", "text": "1.1 g"},
          {"image": "cal", "text": "108 kal"},
          {"image": "salt", "text": "3.1 g"},
          {"image": "fibre", "text": "14.8 g"},
          {"image": "cir", "text": "5"}
        ],
      },
    ]
  },
];

class Meal {
  final String group;
  final Data data;

  Meal({this.group, this.data});

  factory Meal.fromJson(Map<String, dynamic> json) {
    return Meal(group: json["group"], data: Data.fromJson(json["data"]));
  }
}

class Data {
  final String name;
  final String describe;
  final String image;
  final Mineral mineral;

  Data({this.name, this.describe, this.image, this.mineral});

  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
        name: json["name"],
        describe: json["describe"],
        image: "assets/images/" + json["image"] + ".jpg",
        mineral: Mineral.fromJson(json["mineral"]));
  }
}

class Mineral {
  final String image;
  final String text;

  Mineral({this.image, this.text});

  factory Mineral.fromJson(Map<String, dynamic> json) {
    return Mineral(
        image: "assets/image/" + json["image"] + ".png", text: json["text"]);
  }
}
